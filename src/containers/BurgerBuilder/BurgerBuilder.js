import React,{Component} from 'react'
import Auc from '../../hoc/Auc/Auc'
import Burger from '../../components/Burger/Burger'
import BuildControl from '../../components/Burger/BuildControl/BuildControl'
import Modal from '../../components/UI/Modal/Modal'
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary'
import axios from '../../axios-orders'
import Spinner from '../../components/UI/Spinner/Spinner'
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler'


const INGREDIENT_PRICES ={
    salad: 13,
    cheese: 5,
    bacon: 18,
    meat: 25
}
class BurgerBuilder extends Component {
    state = {
        ingredients: null,
        totalPrice:10,
        purshasable: false,
        purchaseClicked: false,
        loading : false,
        error: false
    }
    componentDidMount(){
        axios.get('https://burger-shop-af284.firebaseio.com/ingredients.json')
            .then(response => {
                this.setState({
                    ingredients: response.data
                })
            }).catch(error => {this.setState({
            error: true
        })})
    }

    purchaseClickedHandler = () => {
        this.setState({purchaseClicked:true})
    }

    purchasableHandler (ingredients){
        const sum = Object.keys(ingredients).map(i =>{
            return ingredients[i]
        }).reduce((sum,el)=>{
            return sum+el
        },0)
        this.setState({purshasable:sum > 0})
    }
    addIngredientHandler = (type) =>{
        const oldCount = this.state.ingredients[type]
        const updatedCount = oldCount + 1
        const updatedIngredients = {
            ...this.state.ingredients
        }
        updatedIngredients[type] = updatedCount
        const priceAddition = INGREDIENT_PRICES[type]
        const oldPrice = this.state.totalPrice
        const newPrice = oldPrice+priceAddition
        this.setState({totalPrice:newPrice,ingredients:updatedIngredients})
        this.purchasableHandler(updatedIngredients)
    }
    removeIngredientHandler = (type) => {
        const oldCount = this.state.ingredients[type]
        if(oldCount <= 0){
            return
        }
        const updatedCount = oldCount - 1
        const updatedIngredients = {
            ...this.state.ingredients
        }
        updatedIngredients[type] = updatedCount
        const priceDeduction = INGREDIENT_PRICES[type]
        const oldPrice = this.state.totalPrice
        const newPrice = oldPrice-priceDeduction
        this.setState({totalPrice:newPrice,ingredients:updatedIngredients})
        this.purchasableHandler (updatedIngredients)

    }

    purchaseCancelHandler = () => {
        this.setState({purchaseClicked: false})
    }

    purchaseContinueHandler =() => {
        this.setState({
            loading: true
        })
        const order = {
            ingredients : this.state.ingredients,
            price: this.state.totalPrice,
            customer: {
                name: 'Zhumabek Zalkarbekov',
                adress: {
                    street: 'Parhomenko 130',
                    zipCode: '3434',
                    country: 'Kyrgyzstan'
                },
                email: 'juma@gmail.com'
            },
            deliveryMethod: 'Fast'
        }
        axios.post('/orders.json',order)
            .then(response => {this.setState({
                loading: false, purchaseClicked: false
            })})
            .catch(err => {this.setState({
                loading: false, purchaseClicked: false
            })})
    }

    render(){
        const disabledInfo = {
            ...this.state.ingredients
        }
        for (let i in disabledInfo){
            disabledInfo[i] = disabledInfo[i] <=0
        }
            let orderSummary =  null

            let burger  = this.state.error ? <p>Ingredients can't be loaded</p> : <Spinner/>
            if(this.state.ingredients){
                burger = (
                    <Auc>
                        <Burger ingredients={this.state.ingredients}/>
                        <BuildControl
                            ingredientAdded={this.addIngredientHandler}
                            ingredientRemoved={this.removeIngredientHandler}
                            disabled={disabledInfo}
                            price={this.state.totalPrice}
                            purchasable={this.state.purshasable}
                            ordered={this.purchaseClickedHandler}/>
                    </Auc>
                )
                orderSummary =  <OrderSummary
                    ingredients={this.state.ingredients}
                    price={this.state.totalPrice}
                    purchaseCancelled={this.purchaseCancelHandler}
                    purchaseContinued={this.purchaseContinueHandler}
                />
            }
            if (this.state.loading){
                orderSummary = <Spinner/>
            }




        return(
            <Auc>
                <Modal show={this.state.purchaseClicked} modalClosed={this.purchaseCancelHandler}>
                    {orderSummary}
                </Modal>
                {burger}
            </Auc>
        )
    }

}
export default withErrorHandler(BurgerBuilder, axios)