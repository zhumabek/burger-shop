/**
 * Created by Mr.Robot on 24.12.2017.
 */
import React from 'react'
import Logo from '../Logo/Logo'
import NavItems from '../NavItems/NavItems'
import classes from './SideDrawer.css'
import BackDrop from '../../UI/BackDrop/BackDrop'
import Auc from '../../../hoc/Auc/Auc'

const sideDrawer = (props) => {
    let attachedClasses = [classes.SideDrawer,classes.Close]
    if (props.open){
        attachedClasses = [classes.SideDrawer,classes.Open]
    }

    return(
        <Auc>
            <BackDrop show={props.open} backdropClicked={props.closed}/>
            <div className={attachedClasses.join(" ")}>
                <div className={classes.Logo}>
                    <Logo/>
                </div>
                <nav>
                     <NavItems/>
                </nav>
            </div>
        </Auc>
    )
}

export default sideDrawer