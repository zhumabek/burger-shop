/**
 * Created by Mr.Robot on 24.12.2017.
 */
import React from 'react'
import classes from './DrawerToggle.css'

const drawerToggle = (props) => (
    <div onClick={props.menuClicked} className={classes.DrawerToggle}>
        <div></div>
        <div></div>
        <div></div>
    </div>
)
export default drawerToggle