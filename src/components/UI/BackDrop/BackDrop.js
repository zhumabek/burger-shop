/**
 * Created by Mr.Robot on 14.12.2017.
 */
import React from 'react'
import classes from './BackDrop.css'

const backdrop = (props) => (
    props.show ? <div className={classes.BackDrop} onClick={props.backdropClicked}></div> : null
)

export default backdrop