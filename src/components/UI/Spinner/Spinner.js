/**
 * Created by Mr.Robot on 02.01.2018.
 */
import React from 'react'
import classes from './Spinner.css'

const spinner = () => (
    <div className={classes.Loader}>Loading...</div>
)
export default spinner