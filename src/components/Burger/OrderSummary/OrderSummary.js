/**
 * Created by Mr.Robot on 14.12.2017.
 */
import React from 'react'
import Auc from '../../../hoc/Auc/Auc'
import Button from '../../UI/Button/Button'

const orderSummary = (props) => {
    const ingredientSummary = Object.keys(props.ingredients).map(i => {
        return (<li key={i}><span style={{textTransform: 'capitalize'}}>{i}</span> : {props.ingredients[i]}</li>)
    })
    return(
        <Auc>
            <h3>Your order</h3>
            <p>A delicious burger with the following ingredients: </p>
            <ul>
                {ingredientSummary}
            </ul>
            <p><strong>Total price: {props.price}</strong></p>
            <p>Continue to check out?</p>
            <Button btnType="Danger" clicked={props.purchaseCancelled}>CANCEL</Button>
            <Button btnType="Success" clicked={props.purchaseContinued}>CONTINUE</Button>
        </Auc>
    )
}
export default orderSummary