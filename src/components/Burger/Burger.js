import React from 'react'
import classes from './Burger.css'
import BurgerIng from './BurgerIng/BurgerIng'

const burger = (props) => {
    let transformIngs = Object.keys(props.ingredients).map(ingKey =>{
        return [...Array(props.ingredients[ingKey])].map((_,i) =>{
            return (<BurgerIng key={ingKey+i} type={ingKey}/>)
        })
    }).reduce((arr,el)=>{
        return arr.concat(el)
    },[])

    if(transformIngs.length === 0){
        transformIngs = <p>Please start adding ingredients!!</p>
    }
    return(
        <div className={classes.Burger}>
            <BurgerIng type="top-bread"/>
            {transformIngs}
            <BurgerIng type="bread-bottom"/>
        </div>
    )
}
export default burger
