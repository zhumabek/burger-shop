/**
 * Created by Mr.Robot on 13.12.2017.
 */
import React from 'react'
import classes from './BuldCelement.css'

const buldCelement = (props) => (
    <div className={classes.BuldCelement}>
        <div className={classes.Label}>{props.label}</div>
        <button className={classes.Less} onClick={props.removed} disabled={props.disabled}>Less</button>
        <button className={classes.More} onClick={props.added}>More</button>
    </div>
)
export default buldCelement