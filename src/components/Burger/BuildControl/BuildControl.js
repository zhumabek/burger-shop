/**
 * Created by Mr.Robot on 13.12.2017.
 */
import React from 'react'
import classes from './BuildControl.css'
import BuldCelement from './BuldCelement/BuldCelement'


const controls = [
    {label: 'Salad',type: 'salad'},
    {label: 'Bacon',type: 'bacon'},
    {label: 'Cheese',type: 'cheese'},
    {label: 'Meat',type: 'meat'}
]

const buildControl = (props) => (

        <div className={classes.BuildControl}>
            <p>Current price:<strong>{props.price.toFixed(2)}</strong> som</p>
            {controls.map(ctrl =>(
                <BuldCelement
                    added={() => props.ingredientAdded(ctrl.type)}
                    removed={() => props.ingredientRemoved(ctrl.type)}
                    disabled={props.disabled[ctrl.type]}
                    key={ctrl.label} label={ctrl.label}/>
            ))}
            <button className={classes.OrderButton} disabled={!props.purchasable} onClick={props.ordered}>Order Now</button>
        </div>
)

export default buildControl